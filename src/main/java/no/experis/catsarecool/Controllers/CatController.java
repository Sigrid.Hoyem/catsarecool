package no.experis.catsarecool.Controllers;

import no.experis.catsarecool.CatsAreCoolApplication;
import no.experis.catsarecool.Models.Cat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class CatController {
    @GetMapping("/cat")
        public Cat getCat() throws Exception{
            System.out.println("Trying to find cat");
            return CatsAreCoolApplication.createCat();
        }
}
