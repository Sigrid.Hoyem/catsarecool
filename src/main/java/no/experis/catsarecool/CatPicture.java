package no.experis.catsarecool;

import org.json.JSONObject;

public class CatPicture {
    public static JSONObject getRandomCatPicture() throws Exception{
        String url = "https://aws.random.cat/meow";
        return new JSONObject(CatsAreCoolApplication.requestURL(url));
    }

    public static String getCatPicture(JSONObject jo) throws  Exception{
        return jo.get("file").toString();
    }

    public static void displayCatPicture() throws Exception{
        System.out.println(getCatPicture(getRandomCatPicture()));
    }
}
