package no.experis.catsarecool;

import org.json.JSONException;
import org.json.JSONObject;

public class CatFact {

    public static JSONObject getRandomCatFact() throws Exception{
        String url = "https://cat-fact.herokuapp.com/facts/random";
        return new JSONObject(CatsAreCoolApplication.requestURL(url));
    }
    public static JSONObject getRandomDogFact() throws Exception{
        String url = "https://cat-fact.herokuapp.com/facts/random?animal_type=dog";
        return new JSONObject(CatsAreCoolApplication.requestURL(url));
    }
    public static JSONObject getRandomHorseFact() throws Exception{
        String url = "https://cat-fact.herokuapp.com/facts/random?animal_type=horse";
        return new JSONObject(CatsAreCoolApplication.requestURL(url));
    }
    public static String getRandomAnimalFactTxt(JSONObject jo) throws JSONException {
       return jo.get("text").toString();
    }
    public static String getTypeTxt(JSONObject jo) throws JSONException {
        return jo.get("type").toString();
    }
    public static void displayRandomCat() throws Exception {
        System.out.println("Cat fact:");
        System.out.println(getRandomAnimalFactTxt(getRandomCatFact()));
    }
    public static void displayRandomDog() throws Exception {
        System.out.println("Dog fact:");
        System.out.println(getRandomAnimalFactTxt(getRandomDogFact()));
    }
    public static void displayRandomHorse() throws Exception {
        System.out.println("Horse fact:");
        System.out.println(getRandomAnimalFactTxt(getRandomHorseFact()));
    }

}
