package no.experis.catsarecool;

import no.experis.catsarecool.Models.Cat;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import static no.experis.catsarecool.CatFact.*;
import static no.experis.catsarecool.CatPicture.*;


@SpringBootApplication
public class CatsAreCoolApplication {

	public static String requestURL(String url) throws Exception{
		URLConnection connection = new URL(url).openConnection();
		connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
		connection.connect();
		BufferedReader buffer  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuilder stringBuilder = new StringBuilder();
		String line;
		while ((line = buffer.readLine()) != null) {
			stringBuilder.append(line);
		}
		return stringBuilder.toString();
	}

	public static void displayMenu(){
			System.out.println();
			System.out.println("1. Cat Fact");
			System.out.println("2. Dog Fact");
			System.out.println("3. Horse Fact");
			System.out.println("4. Exit");
			System.out.println();
	}

	public static void menuChoice(int menuChoice) throws Exception{
		switch(menuChoice){
			case 1:
				displayRandomCat();
				displayCatPicture();
				break;
			case 2:
				displayRandomDog();
				break;
			case 3:
				displayRandomHorse();
				break;
			case 4: System.exit(0);
			default:
				break;
		}
	}

	public static void menu() throws Exception{
		Scanner scanner = new Scanner(System.in);
		int menuChoice = 0;
		displayMenu();
			do {
				if(scanner.hasNextInt()) {
					menuChoice = scanner.nextInt();
					menuChoice(menuChoice);
				}else{
					scanner.nextLine();
				}
				displayMenu();
			} while (menuChoice != 4);
			scanner.close();
	}
	public static Cat createCat() throws Exception{
		String fact = getRandomAnimalFactTxt(getRandomCatFact());
		String url = getCatPicture(getRandomCatPicture());
		return new Cat(fact, url);
	}

	public static void main(String[] args) throws Exception{
		SpringApplication.run(CatsAreCoolApplication.class, args);
		createCat();
		menu();
	}


}
