package no.experis.catsarecool.Models;

public class Cat {
    private String fact;
    private String imgUrl;

    public Cat(String fact, String imgUrl){
        this.fact = fact;
        this.imgUrl = imgUrl;
    }

    public String getFact() {
        return fact;
    }

    public void setFact(String fact) {
        this.fact = fact;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
